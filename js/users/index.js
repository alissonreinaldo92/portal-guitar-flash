$(function() {

  $("#btnSaveDescription").click(function() {
    var url = $(this).attr('data-href');
    var description = $('#edtDescription').val();
    var user = $('#edtHiddenUser').attr('value');
    $.post(url, {
      description: description,
      user: user
    },
    function(retorno) {
      $("#postResponse").html(retorno);
      $.jNotify({
        content: "Descrição atualizada com sucesso!",
        timeout: 3000,
        position: 'bottom',
        isClosable: true
      });
    })
            .fail(function(XMLHttpRequest) {
      $("#postResponse").html(XMLHttpRequest.responseText);
      $.jNotify({
        content: "Ops, não foi possível salvar a descrição!",
        timeout: 3000,
        position: 'bottom',
        isClosable: true
      });
    });
  });

  function updateCountdown(textarea) {
    // 140 is the max message length

    var remaining = 5000 - textarea.val().length;
    if (remaining <= 0) {
      textarea.val(textarea.val().substring(0, 5000));
      remaining = 5000 - textarea.val().length;
    }
    $('#countdown').text('Restam ' + remaining + ' caracteres.');
  }

  updateCountdown($('#edtDescription'));
  $('#edtDescription').on('input', function() {
    updateCountdown($('#edtDescription'));
  });

});

