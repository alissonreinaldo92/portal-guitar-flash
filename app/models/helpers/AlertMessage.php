<?php

/**
 * Description of AlertMessages
 *
 * @author Alisson Reinaldo Silva
 */
class AlertMessage {
  
  private $title;
  private $messages = array();
  private $type;

  public function __construct($title, $type = TypeStyleTBAlert::Info) {
    $this->title = $title;
    $this->type = $type;
  }
  
  public function addMessage($message) {
    $this->messages[] = $message;
  }
  
  public function getMessages() {
    return $this->messages;
  }
  
  public function getTitle() {
    return $this->title;
  }
  
  public function getType() {
    return $this->type;
  }
  
  public function setType($type) {
    $this->type = $type;
  }

}

?>
