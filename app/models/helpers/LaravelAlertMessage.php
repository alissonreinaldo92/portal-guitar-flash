<?php

/**
 * Description of LaravelAlertMessage
 *
 * @author Alisson Reinaldo Silva
 */
class LaravelAlertMessage extends AlertMessage {

  public function flash() {
    Session::flash('AlertMessages', $this);
  }

}

?>
