<?php

/**
 * This class converts .sng and .xml game charts to GF2 format
 *
 * @author Alisson Reinaldo Silva
 */
class Conversor implements Validatable {

  private static $instance;
  private static $attributes;
  public static $tempFile;

  /**
   * Método construtor privado para que este só seja acessível dentro da própria classe
   */
  public function __construct() {
    self::$attributes = array();
  }

  /**
   * Converte uma chart do jogo para o formato do GF2
   * @param string $filename O caminho para a chart
   */
  public static function convertChart($file) {

    // Create the DOMDocument
    $doc = new DOMDocument();
    if (get_magic_quotes_runtime()) {
      $file = stripslashes($file);
    }
    $doc->load($file);
    $notes = $doc->getElementsByTagName("Note");
    $lastNoteNode = $notes->item($notes->length - 1); // Pega a última nota
    $songLength = $lastNoteNode->getAttribute('time') + $lastNoteNode->getAttribute('duration') + 3;
// Escreve o cabeçalho da Chart passando o nome da música e artista digitado pelo usuário
    $xml = "<?xml version=\"1.0\"?>\r\n";
    $xml .= "<Song>\r\n";
    $xml .= "    <Properties>\r\n";
    $xml .= "        <Version>0.1</Version>\r\n";
    $xml .= "        <Title>" . self::getAttribute('songName') . "</Title>\r\n";
    $xml .= "        <Artist>" . self::getAttribute('songArtist') . "</Artist>\r\n";
    $xml .= "        <Album>Vazio</Album>\r\n";
    $xml .= "        <Year>0</Year>\r\n";
    $xml .= "        <BeatsPerSecond>24.0</BeatsPerSecond>\r\n";
    $xml .= "        <BeatOffset>0.0</BeatOffset>\r\n";
    $xml .= "        <HammerOnTime>0.25</HammerOnTime>\r\n";
    $xml .= "        <PullOffTime>0.25</PullOffTime>\r\n";
    $xml .= "        <Difficulty>EXPERT</Difficulty>\r\n";
    $xml .= "        <AllowableErrorTime>0.25</AllowableErrorTime>\r\n";
    $xml .= "        <Length>" . $songLength . "</Length>\r\n";
    $xml .= "        <MusicFileName>musica1.mp3</MusicFileName>\r\n";
    $xml .= "        <MusicDirectoryHint>C:\Guitar Flash</MusicDirectoryHint>\r\n";
    $xml .= "    </Properties>\r\n\r\n";
    $xml .= "    <Data>\r\n";

    $array = array();
    $item = 0;

// Cria as notas
    foreach ($notes as $note) {

      $item += 1;

      $noteTime = round(round($note->getAttribute("time") * 24, 0) / 24, 7); // Arredonda o tempo de cada nota para o padrão do jogo
      $noteDuration = round(round($note->getAttribute("duration") * 24, 0) / 24, 7); // Arredonda a duração de cada nota para o padrão do jogo
      $noteTrack = $note->getAttribute("track");

      // Verifica se tem tag special nesta nota e atribui se não houver, do contrário mantém o existente
      if ($note->getAttribute("special")) {
        $noteSpecial = $note->getAttribute("special");
      } else {
        $noteSpecial = "0";
      }

      // Verifica se tem tag special nesta nota e atribui se não houver, do contrário mantém o existente
      if ($note->getAttribute("special")) {
        $noteSpecial = $note->getAttribute("special");
      } else {
        $noteSpecial = "0";
      }
      $array[$item] = array("time" => $noteTime, "duration" => $noteDuration, "track" => $noteTrack, "special" => $noteSpecial);
    }

    for ($i = 1; $i <= $item; $i++) {
      if ($array[$i]["duration"] > 0 && array_key_exists($i + 1, $array) && $array[$i]["time"] <> $array[$i + 1]["time"]) {
        if (($array[$i]["duration"] + $array[$i]["time"]) >= ($array[$i + 1]["time"] - 0.125)) { // Caso a próxima nota esteja a menos de 0.125 de distância da atual (considerando longnote)
          $array[$i]["duration"] = $array[$i + 1]["time"] - 0.125 - $array[$i]["time"]; // Então reduzo o longnote da nota para ficar a pelo menos 0.125 de distância da próxima nota
        }
      } elseif ($array[$i]["duration"] > 0 && array_key_exists($i + 2, $array) && $array[$i]["time"] <> $array[$i + 2]["time"]) {
        if (($array[$i]["duration"] + $array[$i]["time"]) >= ($array[$i + 2]["time"] - 0.125)) {
          $array[$i]["duration"] = $array[$i + 2]["time"] - 0.125 - $array[$i]["time"];
        }
      } elseif ($array[$i]["duration"] > 0 && array_key_exists($i + 3, $array) && $array[$i]["time"] <> $array[$i + 3]["time"]) {
        if (($array[$i]["duration"] + $array[$i]["time"]) >= ($array[$i + 3]["time"] - 0.125)) {
          $array[$i]["duration"] = $array[$i + 3]["time"] - 0.125 - $array[$i]["time"];
        }
      } elseif ($array[$i]["duration"] > 0 && array_key_exists($i + 4, $array) && $array[$i]["time"] <> $array[$i + 4]["time"]) {
        if (($array[$i]["duration"] + $array[$i]["time"]) >= ($array[$i + 4]["time"] - 0.125)) {
          $array[$i]["duration"] = $array[$i + 4]["time"] - 0.125 - $array[$i]["time"];
        }
      }

      $noteTime = $array[$i]['time'];
      $noteTrack = $array[$i]['track'];
      $noteSpecial = $array[$i]['special'];
      $noteDuration = ($array[$i]['duration'] < 0 ? 0 : $array[$i]['duration']);
      $xml .= "        <Note time=\"$noteTime\" duration=\"$noteDuration\" track=\"$noteTrack\" special=\"$noteSpecial\" />\r\n";
    }

// Termina a chart
    $xml .= "\r\n    </Data>\r\n";
    $xml .= "</Song>\r\n";

    $temp = tempnam('temp', 'chart-'); // Cria um arquivo temporário
    if ($temp) {
      $handle = fopen($temp, 'w+');
      if ($handle) {
        fwrite($handle, $xml);
        fclose($handle);
        self::$tempFile = $temp;
        return $temp;
      }
      return false;
    }
    return false;
  }

  public static function getAttribute($name) {
    return self::$attributes[$name];
  }

  /**
   * Operação de classe (estática) que permite acessar a única instância de Conversor
   * @return Conversor
   */
  public static function getInstance() {
    if (self::$instance)
      return self::$instance;
    else {
      self::$instance = new Conversor();

      return self::$instance;
    }
  }

  public function getValidationData() {
    return self::$attributes;
  }

  public function getValidationMessages() {
    return array(
        'file_required' => 'Você precisa selecionar algum arquivo!',
        'file_mimes' => 'O formato do arquivo deve ser .sng ou .xml!',
        'file_max' => 'O arquivo deve ter até 1MB de tamanho!',
        'songName_required' => 'Você precisa escrever algum nome de música!',
        'songName_between' => 'O nome da música deve ter entre 2 e 30 caracteres!',
        'songArtist_required' => 'Você precisa escrever algum nome de banda/artista!',
        'songArtist_between' => 'O nome da banda/artista deve ter entre 2 e 45 caracteres!',
    );
  }

  public function getValidationRules() {
    return array(
        'file' => 'required|mimes:sng,xml|max:1024',
        'songName' => 'required|between:2,30',
        'songArtist' => 'required|between:2,45'
    );
  }

  private static function setAttribute($key, $value) {
    self::$attributes[$key] = $value;
  }

  public function setFile($file) {
    self::setAttribute('file', $file);
  }

  public function setSongArtist($artist) {
    self::setAttribute('songArtist', $artist);
  }

  public function setSongName($name) {
    self::setAttribute('songName', $name);
  }

}

?>
