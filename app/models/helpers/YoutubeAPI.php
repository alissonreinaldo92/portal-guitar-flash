<?php

class YoutubeAPI {

  public static function GetFrame($link) {
    $link = "http://www.youtube.com/embed/" . self::GetID($link);
    return "<iframe width='853' height='480' src='$link' frameborder='0' allowfullscreen></iframe>";
  }

  public static function GetID($link) {
    if (!self::IsYoutube($link)) {
      throw new InvalidArgumentException("$link não é um link válido do Youtube!");
    }
    $posStart = strpos($link, "watch") + 8;
    $posEnd = strpos($link, "&");
    if (is_numeric($posEnd)) {
      $posEnd -= $posStart;
    } else {
      $posEnd = strlen($link) - $posStart;
    }
    return substr($link, $posStart, $posEnd);
  }

  public static function IsYoutube($link) {
    $link = trim($link);
    return (($link <> '') && (stripos($link, "youtube.com") !== false));
  }

}

?>
