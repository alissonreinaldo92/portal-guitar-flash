<?php

/**
 * Description of ValidatorHelper
 *
 * @author Alisson Reinaldo Silva
 */
class ValidatorHelper {

  /**
   * @var \Illuminate\Validation\Validator
   */
  private static $validator;

  /**
   * Cria uma mensagem global com os erros de validação existentes
   */
  private static function flashErrors() {
    $alertMessages = new AlertMessage('Ops! Ocorreu um erro...', TypeStyleTBAlert::Error);
    foreach (self::GetErrorMessages() as $message) {
      $alertMessages->addMessage($message);
    }
    Session::flash('AlertMessages', $alertMessages);
  }

  /**
   * Recupera um array de strings contendo erros de validação.
   * @return	Array
   */
  private static function GetErrorMessages() {
    if (isset(self::$validator)) {
      if (self::$validator instanceof \Illuminate\Validation\Validator) {
        return self::$validator->messages()->all();
      }
      return array();
    }
    return array();
  }

  /**
   * 
   * @param Validatable $object The object to be validated
   * @param array $rules
   * @param callable $callback
   * @return MessageBag
   */
  public static function Validate(Validatable $object, array $rules = array()) {
    $rules = (count($rules) == 0) ? $object->getValidationRules() : $rules;
    $data = $object->getValidationData();
    $messages = $object->getValidationMessages();
    
    self::$validator = Validator::make($data, $rules, $messages);
    $passed = self::$validator->passes();
    if (!$passed) {
      self::flashErrors();
    }
    return $passed;
  }



}

?>
