<?php

/**
 * Description of TypeStyleTBIcons
 *
 * @author Alisson Reinaldo Silva
 */

class TypeStyleTBIcons extends TypeStyleTB {

  const Download = 'icon-download';
  const DownloadWhite = 'icon-download icon-white';
  const Edit = 'icon-pencil';
  const EditWhite = 'icon-pencil icon-white';
  const Exclamation = 'icon-exclamation-sign';
  const Headphone = 'icon-headphones';
  const HeadphoneWhite = 'icon-headphones icon-white';
  const Home = 'icon-home';
  const HomeWhite = 'icon-home icon-white';
  const Music = 'icon-music';
  const MusicWhite = 'icon-music icon-white';
  const PlusSign = 'icon-plus-sign';
  const PlusSignWhite = 'icon-plus-sign icon-white';
  const Search = 'icon-search';
  const Trash = 'icon-trash';
  const User = ' icon-user';
  const UserWhite = ' icon-user icon-white';
  const Wrench = 'icon-wrench';
  const WrenchWhite = 'icon-wrench icon-white';


}


?>
