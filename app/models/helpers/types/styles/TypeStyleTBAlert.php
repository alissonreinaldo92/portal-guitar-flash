<?php

/**
 * Description of TypeStyleTBAlert
 *
 * @author Alisson Reinaldo Silva
 */
class TypeStyleTBAlert extends TypeStyleTB {

  const Alert = 'alert';
  const Error = 'alert-error';
  const Info = 'alert-info';
  const Success = 'alert-success';

}

?>
