<?php

/**
 * Description of TypeStyleTBInput
 *
 * @author Alisson Reinaldo Silva
 */

class TypeStyleTBInput extends TypeStyleTB {

  const Large = 'input-large';
  const Medium = 'input-medium';
  const Small = 'input-small';
  const XLarge = 'input-xlarge';
  const XXLarge = 'input-xxlarge';

}

?>
