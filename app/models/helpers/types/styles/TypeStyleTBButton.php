<?php

/**
 * Description of TypeStyleTBButton
 *
 * @author Alisson Reinaldo Silva
 */
class TypeStyleTBButton extends TypeStyleTB {

  const Base = 'btn';
  const Primary = 'btn-primary';
  const Info = 'btn-info';
  const Warning = 'btn-warning';
  const Success = 'btn-success';
  const Danger = 'btn-danger';
  const Inverse = 'btn-inverse';
  const Link = 'btn-link';
  const Large = 'btn-large';
  const Close = 'close';
  const DropDown = 'caret';
  const Toggle = 'dropdown-toggle';

}

?>
