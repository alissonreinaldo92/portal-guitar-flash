<?php

/**
 * Description of MessageButton
 *
 * @author Alisson Reinaldo Silva
 */
class MessageButton extends Enum {

  const Cancel = 0;
  const No = 1;
  const OK = 2;
  const Yes = 3;
  
  public static $ToString = array(
      'Cancelar',
      'Não',
      'OK',
      'Sim',
  );

}

?>
