<?php

/**
 * Description of ReportType
 *
 * @author Alisson Reinaldo Silva
 */
class ReportType extends Enum {

  const DownloadLink = 'Link de Download de música';
  const RepeatedSong = 'Música repetida';

}

?>
