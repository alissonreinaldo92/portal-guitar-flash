<?php

/**
 * Description of GeneralFunctions
 *
 * @author Alisson Reinaldo Silva
 */
class GeneralFunctions {

  public static final function ConstructTinyUrl($url) {
    $ch = curl_init();
    $timeout = 5;
    curl_setopt($ch, CURLOPT_URL, 'http://tinyurl.com/api-create.php?url=' . $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
  }
  
  public static function Contains($haystack, $needle) {
    return strpos($haystack, $needle) !== false;
  }
  
  public static final function IsLocalhost() {
    $url = Request::Url();
    return strpos($url,'localhost') !== false;
  }

}

?>
