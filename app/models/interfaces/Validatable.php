<?php

/**
 *
 * @author Alisson Reinaldo Silva
 */
interface Validatable {

  /**
   * Recupera todas as properties do objeto em um array, tendo
   * como índice o nome da property, exceto os não escalares.
   * @return	Array
   */
  function getValidationData();

  /**
   * Recupera uma lista de mensagens a serem visualizadas caso o validador detecte algum erro
   * @return    Array
   */
  function getValidationMessages();

  /**
   * Recupera uma lista de regras a serem validadas neste objeto
   * @return    Array
   */
  function getValidationRules();

}

?>
