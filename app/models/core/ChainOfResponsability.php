<?php

/**
 * Description of ChainOfResponsability
 *
 * @author Alisson Reinaldo Silva
 */
abstract class ChainOfResponsability {

  /**
   *
   * @var ChainOfResponsability
   */
  protected $next;

  /**
   *
   * @var Enum
   */
  protected $type;

  /**
   * Constructs the chain, defining the type of object that will try
   * @param Enum $type
   */
  public function __construct() {
    $this->next = null;
  }

  /**
   * Check if the current object can handle the call. Must be defined by each object
   * @return boolean
   */
  abstract protected function canHandle();

  public function execute() {
    if ($this->canHandle()) {
      return $this->handle();
    } elseif (isset($this->next)) {
      return $this->next->execute();
    } else {
      throw new BadMethodCallException('Nenhum objeto da cadeia definida pôde atender à requisição!');
    }
  }

  /**
   * Handle the call, if possible.
   * return mixed
   */
  abstract protected function handle();

  /**
   * 
   * @param ChainOfResponsability $next
   */
  public function setNext(ChainOfResponsability $next) {
    if (is_null($this->next)) {
      $this->next = $next;
    } else {
      $this->next->setNext($next);
    }
  }

  /**
   * Check whether the chain canHandle the request
   * @return boolean
   */
  public function recursiveCanHandle() {
    if (!$this->canHandle()) {
      if (isset($this->next)) {
        return $this->next->recursiveCanHandle();
      }
      return false;
    }
    return true;
  }

}

?>
