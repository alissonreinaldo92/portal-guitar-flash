<?php

/**
 * Implementação do elemento form
 * @author	João Batista Neto
 */
class MainForm extends HTMLComposite {
    
        private $url;
        private $openNamedRoute;

	/**
	 * @param	string $route Route a ser utilizada pelo Form.
	 * @param	string $method Método de envio do formulário.
	 */
	public function __construct( $url, $method = 'post' ) {
		parent::__construct();

		$this->url = $url;
		$this->setMethod($method);
                $this->openNamedRoute = true;
	}
        
	/**
	 * Desenha o Form
	 * @return	string
	 */
	public function Draw() {
            if ($this->openNamedRoute) {
                $this->setRoute($this->url);
            } else {
                $this->setUrl($this->url);
            }
            $form = Form::open($this->attributes);
            $form .= $this->drawChildren();
            $form .= PHP_EOL.Form::close();
            return printf($form);
	}

	/**
	 * Recupera a ação do formulário.
	 * @return	string
	 */
	public function getAction() {
		return $this->getAttribute( 'action' );
	}

	/**
	 * Recupera o método de envio do formulário.
	 * @return	string
	 */
	public function getMethod() {
		return $this->getAttribute( 'method' );
	}
        
	/**
	 * Recupera a Route do Form
	 * @return	string
	 */
	public function getRoute() {
		return $this->getAttribute( 'route' );
	}

	/**
	 * @return	string
	 * @see		HTMLComposite::nodeName()
	 */
	protected function nodeName() {
		return 'form';
	}

	/**
	 * Define a ação do formulário.
	 * @param	string $action A ação do formulário.
	 * @return	Form Uma referência a esse componente.
	 * @see		Component::setAttribute()
	 */
	public function setAction( $action ) {
		return $this->setAttribute( 'action' , $action );
	}

	/**
	 * Define o método de envio do formulário.
	 * @param	string $method O método de envio do formulário.
	 * @return	Form Uma referência a esse componente.
	 * @see		Component::setAttribute()
	 */
	public function setMethod( $method ) {
		return $this->setAttribute( 'method' , $method );
	}
        
	/**
	 * Define a Route do Form
	 * @param	string $route A route a ser utilizada pelo Form
	 * @return	Form Uma referência a esse componente.
	 */
	public function setRoute($route) {
		return $this->setAttribute( 'route' , $route );
	}
        
	/**
	 * Define a URL do Form
	 * @param	string $url A URL a ser utilizada pelo Form
	 * @return	Form Uma referência a esse componente.
	 */
	public function setUrl($url) {
		return $this->setAttribute( 'url' , $url );
	}
        
	/**
	 * Faz com que o Form tente usar a $url como uma Rota Nomeada
	 * @return	Form Uma referência a esse componente.
	 */
        public function useNamedRoute() {
            $this->openNamedRoute = true;
            return $this;
        }
        
	/**
	 * Faz com que o Form use a própria $url
	 * @return	Form Uma referência a esse componente.
	 */
        public function useUrl() {
            $this->openNamedRoute = false;
            return $this;
        }
}