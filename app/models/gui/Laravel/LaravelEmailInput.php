<?php

/**
 * Implementação do elemento input
 * @author	Alisson Reinaldo Silva
 */
class LaravelEmailInput extends LaravelInput {

  public function __construct() {
    parent::__construct();
    $this->type = 'email';
  }

}