<?php

/**
 * Implementação do elemento input
 * @author	Alisson Reinaldo Silva
 */
class TextInput extends FormInput {

  public function __construct($name) {
    parent::__construct($name);
    $this->setType('text');
    $this->addStyle(TypeStyleTBInput::Large);
  }

}