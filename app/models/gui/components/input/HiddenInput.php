<?php

/**
 * Implementação do elemento input
 * @author	Alisson Reinaldo Silva
 */
class HiddenInput extends FormInput {

  public function __construct($name) {
    parent::__construct($name);
    $this->setType('hidden');
  }

}