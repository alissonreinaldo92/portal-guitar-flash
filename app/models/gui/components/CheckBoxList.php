<?php

/**
 * Description of CheckBoxList
 *
 * @author Alisson Reinaldo Silva
 */
class CheckBoxList extends HTMLComposite {

  private $items = array();

  public function __construct() {
    parent::__construct();
    $this->addStyle('checkbox-inline');
  }

  public function addItem($caption, $name) {
    $label = new Label($caption, $name);
    $label->addStyle('checkbox inline');
    $label->addChild(new CheckBoxInput($caption, $name));
    $this->items[$name] = $label;
  }

  public function draw() {
    $result = '';
    foreach ($this->items as $item) {
      $result .= $item->draw().PHP_EOL;
    }
    return $result;
  }

  protected function nodeName() {
    return '';
  }

}

?>
