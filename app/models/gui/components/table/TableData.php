<?php

/**
 * Description of TableData
 *
 * @author Alisson Reinaldo Silva
 */
class TableData extends TableCell {

  public function __construct(\Component $content, $id = null) {
    parent::__construct($content, $id);
    $this->addStyle('pointer');
    $this->addStyle('clickable');
  }

  protected function nodeName() {
    return 'td';
  }

}

?>
