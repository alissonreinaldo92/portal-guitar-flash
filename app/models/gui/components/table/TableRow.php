<?php

/**
 * Description of TableRow
 *
 * @author Alisson Reinaldo Silva
 */
class TableRow extends HTMLComposite {

  private $cells = array();
  private $link;

  /**
   * Adiciona uma nova célula à linha
   * @param Component $child
   * @return TableCell
   * @throws MethodNotAllowedException se o filho não for uma célula
   */
  public function addChild(Component $child) {
    if ($child instanceof FormInput) {
      return parent::addChild($child);
    }
    if ($child instanceof TableCell) {
      if (is_null($child->getAttribute('id')))
        $child->generateId();
      $this->cells[$child->getId()] = $child;
      return parent::addChild($child);
      $this->notify();
    }
    throw new InvalidArgumentException('TableRow só recebe TableCell ou FormInput como filhos');
  }

  /**
   * Cria uma nova célula com um texto
   * @param Component $content
   * @return TableData
   */
  public function addData(Component $content, $id = null) {
    return $this->addChild(new TableData($content, $id));
  }
  
  public function addDrawable(Component $drawable, $id = null) {
    $this->addText($drawable->draw(), $id);
  }

  public function addText($text, $id = null) {
    return $this->addChild(new TableData(new Text($text), $id));
  }

  /**
   * Cria um novo cabeçalho
   * @param type $caption
   * @return TableColumnHeader
   */
  public function addHeader($caption) {
    return $this->addChild(new TableColumnHeader(new Text($caption)));
  }

  /**
   * Retorna todas as células desta linha
   * @return array
   */
  public function getCells() {
    return $this->cells;
  }

  public function hasCell($id) {
    return array_key_exists($id, $this->cells);
  }

  protected function nodeName() {
    return 'tr';
  }

  /**
   * Remove um filho, notificando seus Observers
   * @param \Component $child
   */
  public function removeChild(\Component $child) {
    parent::removeChild($child);
    $this->notify();
  }

}

?>
