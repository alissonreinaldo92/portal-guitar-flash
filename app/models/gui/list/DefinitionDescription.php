<?php

/**
 * Implementação do elemento DD
 * @author	João Batista Neto
 */
class DefinitionDescription extends HTMLComposite {
	/**
	 * @return	string
	 * @see		HTMLComposite::nodeName()
	 */
	protected function nodeName() {
		return 'dd';
	}
}