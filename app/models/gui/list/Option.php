<?php

/**
 * Description of Option
 *
 * @author Alisson Reinaldo Silva
 */
class Option extends HTMLComposite {
  
  public $checked = false;

  public function __construct($caption, $value = null) {
    parent::__construct();
    $this->addChild(new Text($caption));
    if ($value !== null) {
      $this->setAttribute('value', $value);
    }
  }
  
  public function draw() {
    if ($this->checked)
      $this->setAttribute ('selected', 'selected');
    return parent::draw();
  }

  public function nodeName() {
    return 'option';
  }

}

?>
