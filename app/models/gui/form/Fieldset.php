<?php

/**
 * Implementação do elemento fieldset
 * @author	João Batista Neto
 */
class Fieldset extends HTMLComposite {
	/**
	 * @return	string
	 * @see		HTMLComposite::nodeName()
	 */
	protected function nodeName() {
		return 'fieldset';
	}
}