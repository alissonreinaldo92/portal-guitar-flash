<?php

/**
 * Implementação do elemento input
 * @author	João Batista Neto
 */
class Legend extends HTMLComposite {
	/**
	 * @return	string
	 * @see		HTMLComposite::nodeName()
	 */
	protected function nodeName() {
		return 'legend';
	}
}