<?php

/**
 * Implementação do elemento span
 * @author	Alisson Reinaldo Silva
 */
class Span extends HTMLComposite {

  private static $columnsCount = 12;
  private $offset;
  private $size = 0;
  private $supportedAlignmentPositions = array('left', 'right');

  /**
   * Alinha o Span para a posição desejada
   * @param string $position Para qual lado deseja-se alinhar o Span
   * @return \Span Uma referência ao próprio objeto
   * @throws BadMethodCallException Quando o tipo passado como parâmetro não for suportado
   */
  public function Align($position) {
    if (!in_array($position, $this->supportedAlignmentPositions)) {
      throw new BadMethodCallException('Posição de alinhamento de Span não suportado');
    }
    if ($this->size == 0) {
      throw new BadMethodCallException('Para alinhar um Span, ele precisa ter um tamanho diferente de 0');
    }
    if ($position == 'left') {
      $this->setOffset(0);
    }
    if ($position == 'right') {
      $this->setOffset(Span::$columnsCount - $this->size);
    }
    return $this;
  }

  /**
   * 
   * @param Int $size O tamanho do Span
   */
  public function __construct($size = 0) {
    parent::__construct();
    $this->setSize($size);
  }

  /**
   * @return	string
   * @see		HTMLComposite::nodeName()
   */
  protected function nodeName() {
    return 'span';
  }

  /**
   * Cria um espaço vazio à esquerda do Span.
   * @param Int $offset O tamanho do offset
   * @return \Span Uma referência ao próprio Componente
   * @throws BadMethodCallException Quando o tamanho não for entre 0 e a quantidade de colunas possíveis menos um, ou não for um inteiro
   */
  public function setOffset($offset) {
    if (!is_int($offset) || ($offset < 0 || $offset > Span::$columnsCount - 1)) {
      throw new BadMethodCallException('O tamanho do offset do Span deve ser de 0 a ' . Span::$columnsCount - 1 . '. $offset dado.');
    }
    $this->offset = $offset;
    $this->addStyle(TypeStyleTB::Offset . $offset);
    return $this;
  }

  /**
   * Define o tamanho do Span
   * @param int $size O tamanho do Span
   * @return \Span Uma referência ao próprio Componente
   * @throws BadMethodCallException Quando o tamanho não for entre 0 e a quantidade de colunas disponíveis ou não for um inteiro
   */
  public function setSize($size) {
    if (!is_int($size)) {
      throw new BadMethodCallException('O tamanho do Span deve ser um inteiro válido. ' . $size . ' dado');
    }
    if ($size < 0 || $size > Span::$columnsCount) {
      throw new BadMethodCallException('O tamanho do Span deve de 0 a ' . Span::$columnsCount . '. ' . $size . ' dado.');
    }
    if ($this->size > 0) {
      $this->removeStyle(TypeStyleTBSpan::span . $this->size);
    }
    $this->size = $size;
    if ($size > 0) {
      $this->addStyle('span' . $size);
    }
    return $this;
  }

}