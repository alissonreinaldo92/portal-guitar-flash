<?php

/**
 * Implementação de uma imagem
 * @author	João Batista Neto
 */
class Image extends Component {

  private $alt;
  private $height;
  private $src;
  private $width;

  /**
   * @param	string $src
   * @param	string $alt
   */
  public function __construct($src, $alt = null) {
    parent::__construct();
    $this->src = $src;
    $this->alt = $alt;
  }

  /**
   * @return	string
   * @see		Component::draw()
   */
  public function draw() {
    $this->setAttribute('src', asset($this->src));
    if ($this->alt != null) {
      $this->setAttribute('alt', $this->alt);
    }
    return sprintf('<img%s />', $this->drawAttributes());
  }

  /**
   * Retorna a URL da imagem
   * @return string
   */
  public function getSrc($asset = false) {
    return ($asset ? asset($this->src) : $this->src);
  }

  /**
   * Define a altura de uma Imagem.
   * @param int $size A altura da Imagem.
   * @return Image uma referência ao próprio objeto.
   * @throws BadMethodCallException Se o valor não for um inteiro válido.
   */
  public function setHeight($size) {
    if (!is_int($size)) {
      throw new BadMethodCallException('A altura da Imagem precisa ser um inteiro válido. ' . $size . ' dado.');
    }
    $this->height = $size;
    $this->setAttribute('style', 'width:' . $this->width . 'px;height:' . $this->height . 'px;');
    return $this;
  }

  /**
   * Define a largura de uma Imagem.
   * @param int $size A largura da Imagem.
   * @return Image uma referência ao próprio objeto.
   * @throws BadMethodCallException Se o valor não for um inteiro válido.
   */
  public function setWidth($size) {
    if (!is_int($size)) {
      throw new BadMethodCallException('A largura da Imagem precisa ser um inteiro válido. ' . $size . ' dado.');
    }
    $this->width = $size;
    $this->setAttribute('style', 'width:' . $this->width . 'px;height:' . $this->height . 'px;');
    return $this;
  }

}