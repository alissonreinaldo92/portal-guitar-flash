<?php

/**
 * Description of Band
 *
 * @author Alisson Reinaldo Silva
 */
class Band extends EntityObject {
  
  public function getImage() {
    $filename = 'img/bands/' . $this->id . '.jpg';
    if (!file_exists($filename)) {
      $filename = OpenGraph::getInstance()->getImage()->getSrc();
    }
    $image = new Image(asset($filename), 'BandPhoto');
    $image->setWidth(800)->setHeight(800);
    $image->addStyle('circular');
    return $image;
  }
  
  public function getLevelAverage() {
    $songs = $this->songs()->where('level', '<>', 0);
    $result = $songs->avg('level');
    $result = ($result == 0 ? 'Indefinida' : round($result, 2));
    return $result;
  }
  
  public function getSongCount() {
    return $this->songs()->count();
  }
  
  public function getSongDownloadCount() {
    $songs = $this->songs();
    return $songs->sum('downloads');
  }

  public function getValidationMessages() {
    return array(
        'name.required' => 'Precisamos saber qual o artista/banda!',
        'name.min' => 'O artista/banda precisa ter ao menos 2 caracteres!',
        'name.max' => 'O artista/banda não pode ter mais do que 50 caracteres!',
        'name.regex' => 'O artista/banda só pode conter letras, números e espaços!',
        'name.unique' => 'Não é possível criar novo registro para a banda '.$this->name.'. Já existe registro!'
    );
  }
  
  public function getValidationRules() {
    return array(
        'name' => 'required|unique:bands|min:2|max:50|regex:/^([a-z0-9\x20])+$/i',
    );
  }
  
  public function songs() {
    return $this->hasMany('Song');
  }

}

?>
