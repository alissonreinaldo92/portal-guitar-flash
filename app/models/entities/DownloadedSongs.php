<?php

/**
 * Description of DownloadedSongs
 *
 * @author Alisson Reinaldo Silva
 */
class DownloadedSongs extends EntityObject {

  protected $table = 'downloaded_songs';

  public static function boot() {
    parent::boot();
    $class = __CLASS__;
    $class::creating(function($model) {
              $model->OnCreating();
            });
  }
  
  /**
   * Event disparado quando estamos inserindo um novo usuário. Primeiro validamos antes de
   * inserir, portanto, na validação a senha do usuário ainda não está com Hash. Antes de
   * inserir, fazemos Hash nela, guardando a senha original em uma property.
   * @param Model $model
   */
  public function OnCreating() {
    if (!(isset($this->user) && isset($this->song))) {
      return false;
    }
    if (DownloadedSongs::where('user', '=', $this->user)->where('song', '=', $this->song)->count() > 0) {
      throw new InvalidArgumentException('Este usuário já fez Download desta música');
    } 
  }

}

?>
