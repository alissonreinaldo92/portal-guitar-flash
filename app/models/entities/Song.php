<?php

/**
 * Description of Song
 *
 * @author Alisson Reinaldo Silva
 */
class Song extends EntityObject {

  /**
   * Gets the song's band
   * @return Band
   */
  public function band() {
    return $this->belongsTo('Band');
  }

  /**
   * Returns the belonged user instance of this song.
   * @return User
   */
  public function user() {
    return $this->belongsTo('User');
  }

  public function getValidationMessages() {
    return array(
        'name.required' => 'Precisamos saber o nome da música!',
        'name.min' => 'A música precisa ter ao menos 2 caracteres!',
        'name.max' => 'A música não pode ter mais do que 50 caracteres!',
        'name.regex' => 'A música só pode conter letras, números e espaços!',
        'level.integer' => 'Defina algum level de dificuldade para a música!',
        'link.required' => 'Você precisa fornecer um link de download da música!',
        'link.min' => 'O link de download precisa ter ao menos 3 caracteres!',
        'link.max' => 'O link de download não pode ter mais do que 255 caracteres!',
        'link.active_url' => 'O link de download fornecido não é válido!',
        'link.unique' => 'O link de download fornecido já pertence a outra música!',
        'video.min' => 'O link do vídeo precisa ter ao menos 3 caracteres!',
        'video.max' => 'O link do vídeo não pode ter mais do que 255 caracteres!',
        'video.active_url' => 'O link do vídeo fornecido não é válido!',
        'video.unique' => 'O link do vídeo fornecido já pertence a outra música!',
    );
  }

  public function getValidationRules() {
    return array(
        'name' => 'required|min:2|max:50|regex:/^[\w\s!,.-]*$/u',
        'level' => 'required|integer',
        'link' => 'required|min:5|max:510|unique:songs,link,'.$this->id,
        'video' => 'min:5|max:510|unique:songs,video,'.$this->id
    );
  }

  public function setBand(Band $band) {
    $this->band_id = $band->getId();
  }
  
  protected function setLinkAttribute($value) {
    if ($value <> '') {
      $value = GeneralFunctions::ConstructTinyUrl($value);
    }
    $this->attributes['link'] = $value;
  }

  public function setUser(User $user) {
    $this->user_id = $user->getId();
  }
  
//  protected function setVideoAttribute($value) {
//    if ($value <> '') {
//      $value = GeneralFunctions::ConstructTinyUrl($value);
//    }
//    $this->attributes['video'] = $value;
//  }

}

?>
