<?php

/**
 * Description of ObjectSettings
 *
 * @author Alisson Reinaldo Silva
 */
class ObjectConfiguration extends EntityObject {
  
  protected $table = 'object_configurations';

  /**
   * 
   * @param Configurable $object The object to get configured
   * @param SettingsType $settingType The type of configuration to be done
   * @param mixed $value Any value
   */
  public function __construct(Configurable $object, SettingsType $settingType, $value) {
    $this->class = $object->getClass();
    $this->object = $object->getId();
    $this->setting = $settingType;
    $this->value = $value;
    $this->save();
  }

}

?>
