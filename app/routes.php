<?php

Route::get('bands', array('as' => 'bands.index', 'uses' => 'BandsController@index'));
Route::any('bands/{id}', array('as' => 'bands.show', 'uses' => 'BandsController@show'));

Route::any('conversor', array('as' => 'conversor.index', 'uses' => 'ConversorController@index'));
Route::any('conversor/converter', array('as' => 'conversor.converter', 'uses' => 'ConversorController@converter'));

Route::any('perfil', array('as' => 'users.index', 'before' => 'auth', 'uses' => 'UsersController@index'));
Route::any('perfil/{id}', array('as' => 'users.show', 'uses' => 'UsersController@show'));
Route::post('user/edit', array('as' => 'users.edit', 'before' => 'auth', 'uses' => 'UsersController@edit'));
Route::any('users', array('as' => 'users.list', 'uses' => 'UsersController@lista'));
Route::any('users/search', array('as' => 'users.searchList', 'uses' => 'UsersController@searchList'));

Route::any('cadastrar', array('as' => 'users.register', 'before' => 'unlogged', 'uses' => 'UsersController@cadastro'));
Route::post('cadastrar/submit', array('as' => 'users.create', 'uses' => 'UsersController@inserir'));
Route::post('login', array('as' => 'users.login', 'uses' => 'UsersController@login'));
Route::any('logout', array('as' => 'users.logout', function() {
      try {
        $user = LoginChain::getLoggedUser();
        $user->Logout();
      } catch (Exception $exc) {
        echo 'Não há usuário logado!';
      }
      $lastRouteAction = Session::get('LastRouteAction');
      return Redirect::to($lastRouteAction);
    }));

//Route::any('guitarflashdadepressao', array('as' => 'guitarflashdepressao.index', 'uses' => 'GuitarFlashDepressaoController@index'));
    
Route::any('/', array('as' => 'Home', 'uses' => 'HomeController@index'));
Route::get('password/remind', array('as' => 'password.remind', 'uses' => 'HomeController@remind'));
Route::post('password/remind', array('as' => 'password.request', 'uses' => 'HomeController@request'));
Route::get('password/reset/{token}', array('as' => 'password.reset', 'uses' => 'HomeController@reset'));
Route::post('password/reset/{token}', array('as' => 'password.update', 'uses' => 'HomeController@update'));

Route::any('reports', array('as' => 'reports.index', 'uses' => 'ReportsController@index'));
Route::post('reports/submit', array('as' => 'reports.insert', 'uses' => 'ReportsController@insert'));

Route::any('songs', array('as' => 'songs.index', 'uses' => 'SongsController@index'));
Route::any('songs/search', array('as' => 'songs.search', 'uses' => 'SongsController@search'));
Route::any('songs/delete/{id}', array('as' => 'songs.delete', 'uses' => 'SongsController@delete'));
Route::any('songs/edit/{id}', array('as' => 'songs.edit', 'uses' => 'SongsController@edit'));
Route::post('songs/inserir', array('as' => 'songs.insert', 'before' => 'auth', 'uses' => 'SongsController@save'));
Route::post('songs/update/{id}', array('as' => 'songs.update', 'before' => 'auth', 'uses' => 'SongsController@save'));
Route::any('songs/show/{id}', array('as' => 'songs.show', 'uses' => 'SongsController@show'));
Route::post('songs/download', array('as' => 'songs.download', function() {
      if (Request::ajax()) {
        $song = Song::find(Input::get('song')); // Get the downloaded song
        $idUser = Input::get('user'); // Check whether or not we had an User when the song was Downloaded
        if (is_numeric($idUser) && $idUser > 0) {
          $user = User::find($idUser); // If yes, we get this User and assossiate them with the song
          $user->downloaded($song); // Inside this association, if this user has never downloaded this song, we increment the songs download property
        } else {
          $song->downloads += 1; // Otherwise, we just increment the song's downloads count
        }
        $song->save();
        return $song->downloads;
      }
    }));
Route::any('service/songs/get/{id}', array('as' => 'service.song', 'uses' => 'WebServiceController@getSong'));