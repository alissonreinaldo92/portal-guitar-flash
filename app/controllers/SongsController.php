<?php

/**
 * Description of Songs
 *
 * @author Alisson Reinaldo Silva
 */
class SongsController extends BaseController {

  public function index($songs = null) {
    Input::flash();
    if (LoginChain::HasLoggedUser()) {
      $this->AddView('insert');
    } else {
      $this->AddView('notlogged');
    }
    $this->AddView('search');
    if (is_null($songs))
      $songs = Song::join('users', 'users.id', '=', 'songs.user_id')
            ->join('bands', 'bands.id', '=', 'songs.band_id')
            ->select(['songs.id', 'songs.name AS song_name', 'users.name AS user_name', 'bands.name AS band_name', 'songs.level', 'songs.user_id', 'songs.created_at'])
            ->orderBy('songs.created_at', 'desc')
            ->paginate(20);
    $this->AddView('list')->songs = $songs;
    $this->AddScript('js/songs/index.js');
    return $this->ConstructView();
  }

  public function delete($id) {
    $song = Song::find($id);
    $user = LoginChain::getLoggedUser();
    if ($user->getId() == $song->user_id) {
      Song::destroy($id);
      $alert = new LaravelAlertMessage('Sucesso!', TypeStyleTBAlert::Success);
      $alert->addMessage('Chart foi deletada com sucesso.');
      $alert->flash();
      return Redirect::route('users.index');
    }
    else
      Redirect::route('Home');
  }

  public function edit($id) {
    try {
      $song = Song::find($id);
      $user = LoginChain::getLoggedUser();
      if (($user->getId() == $song->user_id) || $user->hasSkill(SkillType::Admin)) {
        $this->AddView('edit')->song = $song;
        return $this->ConstructView();
      }
      else {
        $alert = new LaravelAlertMessage('Ops!', TypeStyleTBAlert::Success);
        $alert->addMessage('Esta chart não pertence à você, você não pode editá-la.');
        $alert->flash();
        Redirect::route('songs.index');
      }
    } catch (Exception $exc) {
      return Redirect::route('songs.index');
    }
  }

  /**
   * Build and execute a query basing on parameters.
   * @param Array $params
   * @return \Illuminate\Pagination\Paginator
   */
  public static function executeSearch($params = array()) {
    
    $artist = array_key_exists('artist', $params) ? $params['artist'] : '';
    $name = array_key_exists('name', $params) ? $params['name'] : '';
    $levels = array_key_exists('levels', $params) ? $params['levels'] : array();
    $order = array_key_exists('order', $params) ? $params['order'] : 'name';
    $direction = array_key_exists('direction', $params) ? $params['direction'] : '';
    $qtd = array_key_exists('quantity', $params) ? $params['quantity'] : 20;
    $user_id = array_key_exists('user_id', $params) ? $params['user_id'] : 0;
    
    $query = Song::join('users', 'users.id', '=', 'songs.user_id')
          ->join('bands', function($join) use ($artist){
                $join->on('bands.id', '=', 'songs.band_id');
                if (trim($artist)) 
                  $join->on('bands.name', 'like', DB::raw('\'%' . $artist . '%\''));
                          });

    if (trim($name))
      $query->where('songs.name', 'like', "%$name%");
    
    if (trim($user_id))
      $query->where('songs.user_id', '=', "$user_id");

    if (count($levels) > 0)
      $query->whereIn('songs.level', $levels);

    // Soft Deletes
    $query->whereNull('songs.deleted_at')->whereNull('users.deleted_at')->whereNull('bands.deleted_at');

    $query->orderBy("songs.$order", $direction);
    $songs = $query->select(['songs.id', 'songs.name AS song_name', 'users.name AS user_name', 'bands.name AS band_name', 'songs.level', 'songs.user_id', 'songs.created_at'])->paginate($qtd);
    return $songs; // Return the index action of the SongsController with the songs list
  }

  public function save($id = null) {
    Input::flash();
    if (is_null($id)) {
      $song = new Song;
    } else {
      $song = Song::find($id);
    }
    $song->setUser(LoginChain::getLoggedUser());
    $song->name = trim(Input::get('edtName'));
    $name = trim(Input::get('edtArtist'));
    $band = Band::exists('name', $name);
    if (!$band) {
      $band = new Band;
      $band->name = $name;
      if (ValidatorHelper::Validate($band)) {
        $band->save();
      } else {
        return Redirect::to('songs')->withInput();
      }
    }
    $song->setBand($band);
    $song->level = Input::get('cbbLevel');
    $song->link = Input::get('edtLink');
    $song->video = Input::get('edtVideo');
    if (ValidatorHelper::Validate($song)) {
      $song->save();
      $alert = new LaravelAlertMessage('Sucesso!', TypeStyleTBAlert::Success);
      $alert->addMessage('Sua chart foi salva com sucesso. Parabéns, e continue contribuindo!');
      $alert->flash();
      return Redirect::route('songs.show', array($song->getId()));
    } else {
      if (Session::has('LastRouteAction')) {
        $lastRouteAction = Session::get('LastRouteAction');
        return Redirect::to($lastRouteAction);
      } else {
        return Redirect::to('songs')->withinput();
      }
    }
  }

  public function search() {

    $params = array();
    Session::reflash();
    $params['name'] = Input::get('edtSong', Session::get('songs.search.name'));
    $params['artist'] = Input::get('edtArtist', Session::get('songs.search.artist'));
    $params['quantity'] = Input::get('cbbQtd', Session::get('songs.search.qtd'));
    $params['order'] = Input::get('cbbOrder', Session::get('songs.search.order'));
    $params['direction'] = Input::get('cbbDirection', Session::get('songs.search.direction'));
    // Persist data on Session
    Session::flash('songs.search.name', $params['name']);
    Session::flash('songs.search.artist', $params['artist']);
    Session::flash('songs.search.qtd', $params['quantity']);
    Session::flash('songs.search.order', $params['order']);
    Session::flash('songs.search.direction', $params['direction']);
    $params['levels'] = array();
    for ($i = 1; $i <= 7; $i++) {
      $level = Input::get("chk$i", Session::get("songs.search.chk$i"));
      Session::put("songs.search.chk$i", $level);
      if ($level == 'on')
        $params['levels'][] = $i;
    }
    $songs = self::executeSearch($params);
    $count = $songs->count();
    if ($count > 0) {
      $alert = new LaravelAlertMessage('Sucesso!', TypeStyleTBAlert::Success);
      $alert->addMessage("$count chart(s) localizada(s)!");
      $alert->flash();
    } else {
      $error = new LaravelAlertMessage('Ops!', TypeStyleTBAlert::Info);
      $error->addMessage('Infelizmente não localizamos registros com os parâmetros desejados!');
      $error->addMessage('Tente retirar um ou mais parâmetros e efetue a busca novamente!');
      $error->addMessage('Por agora, trouxemos as últimas charts!');
      $error->flash();
      return Redirect::route('songs.index')->withinput();
    }

    return $this->index($songs);

  }

  public function searched() {
    try {
      $query = Session::get('songs.query');
      $songs = $query->get();
      Session::flash();
    } catch (Exception $exc) {
      
    }
    return $this->index($songs); // Return the index action of the SongsController with the songs list
  }

  public function show($id) {
    Input::flash();
    $song = Song::find($id);
    if (is_null($song)) {
      $error = new LaravelAlertMessage('Chart não encontrada', TypeStyleTBAlert::Info);
      $error->addMessage('Desculpe-nos, mas não foi possível localizar a chart desejada!');
      $error->flash();
      return Redirect::route('songs.index');
    }
    if (LoginChain::HasLoggedUser()) {
      $this->AddView('insert');
    } else {
      $this->AddView('notlogged');
    }
    App::instance('Song', $song);
    $this->AddView('search');
    $this->AddView('show');
    $this->AddView('disqus', 'base');
    $this->AddScript('js/songs/index.js');
    $this->AddScript('js/songs/show.js');
//    if ($new) // It doesn't work yet
//      $this->AddScript('js/songs/show.sharer.js');
    Session::flash('LastRouteActionParameters', $id);

    $path = Request::path();

    $facebook = FacebookAPI::getInstance();
    $facebook->setPath($path);

    $og = OpenGraph::getInstance();
    $og->setImageUrl($song->band->getImage()->getSrc());
    $og->setPath($path);
    $og->setTitle('Portal do Guitar Flash - Música para Download: ' . $song->name . ' by ' . $song->band->name);

    return $this->ConstructView();
  }

}

?>
