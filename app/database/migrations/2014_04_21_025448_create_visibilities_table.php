<?php

use Illuminate\Database\Migrations\Migration;

class CreateVisibilitiesTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    if (!Schema::hasTable('visibilities')) {
      Schema::create('visibilities', function($table) {
                $table->integer('id')->unsigned();
                $table->primary('id');
                $table->string('description', 50);
                $table->timestamps();
                $table->softDeletes();
              });
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }

}