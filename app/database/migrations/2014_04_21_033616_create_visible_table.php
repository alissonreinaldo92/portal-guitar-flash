<?php

use Illuminate\Database\Migrations\Migration;

class CreateVisibleTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    if (!Schema::hasTable('visible')) {
      Schema::create('visible', function($table) {
                $table->integer('visibility_id')->unsigned();
                $table->integer('visible_id')->unsigned();
                $table->string('visible_type', 50);
                $table->foreign('visibility_id')->references('id')->on('visibilities');
              });
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }

}