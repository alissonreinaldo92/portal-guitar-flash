<?php

use Illuminate\Database\Migrations\Migration;

class CreateFacebookUsersTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    if (!Schema::hasTable('facebook_users')) {
      Schema::create('facebook_users', function($table) {
                $table->string('id', 100)->unique();
                $table->integer('user_id')->unsigned()->unique();
                $table->primary('fb_id');
                $table->foreign('user_id')->references('id')->on('users');
                $table->timestamps();
                $table->softDeletes();
              });
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    //
  }

}