<?php

use Illuminate\Database\Migrations\Migration;

class CreatePasswordRemindersTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    if (!Schema::hasTable('password_reminders')) {
      Schema::create('password_reminders', function($t) {
                $t->string('email');
                $t->string('token');
                $t->timestamp('created_at');
              });
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {

  }

}
