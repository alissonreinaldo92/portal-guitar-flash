@section('bands.list')

<h2>Lista de Bandas</h2>
<?php
$table = new Table();
$table->addStyle(TypeStyleTBTable::Zebra);
$table->addStyle(TypeStyleTBTable::Highlight);
$table->addStyle(TypeStyleTBTable::Compact);

$table->getTableHead()->getRow()->addHeader('Nome');
$table->getTableHead()->getRow()->addHeader('Qtd');

foreach ($bands as $band) {

  $table->getTableBody()->addRow()->addText($band->name);
  $table->getTableBody()->getLastRow()->addText($band->getSongCount());
  $table->getTableBody()->getLastRow()->setId($band->getId());
  $table->getTableBody()->getLastRow()->addStyle('pointer');
  $table->getTableBody()->getLastRow()->addStyle('clickable');

  $url = URL::Route('bands.show', $band->getId());
  $input = new HiddenInput('inpBandLink');
  $input->setValue($url);

  $table->getTableBody()->getLastRow()->addChild($input);
}

echo $table;

echo $bands->links();

?>
@stop