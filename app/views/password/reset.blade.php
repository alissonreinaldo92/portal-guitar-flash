@section('resetpassword')

@if (Session::has('error'))
<?php
$errorMessage = new AlertMessage('Digite sua nova senha abaixo!', TypeStyleTBAlert::Error);
if (Session::get('reason') == 'reminders.user') {
$errorMessage->addMessage('O e-mail fornecido não foi localizado!');
} elseif (Session::get('reason') == 'reminders.password') {
$errorMessage->addMessage('Houve algum problema com a(s) senha(s) fornecida(s)!');
} elseif (Session::get('reason') == 'reminders.token') {
$errorMessage->addMessage('Esta requisição não é válida, e/ou não foi provida pelo nosso sistema!');
} else {
$errorMessage->addMessage(Session::get('reason'));
} 
$alertError = new Alert();
$alertError->setAlertMessage($errorMessage);
echo $alertError->Draw();
?>
@endif

<?php
$alertMessage = new AlertMessage('Digite sua nova senha abaixo!', TypeStyleTBAlert::Info);
$alertMessage->addMessage('Após concluir esta ação, sua senha será resetada.');
$alert = new Alert();
$alert->setAlertMessage($alertMessage);
echo $alert->Draw();
echo '</br>';

$button = new Button('Resetar', 'btnEnviar');
$button->addStyle(TypeStyleTBButton::Primary);
$button->SetType('submit');

$email = new EmailInput('email');
$email->setPlaceholder('Digite seu e-mail');

$password = new PasswordInput('password');
$password->setPlaceholder('Nova Senha');

$passwordConf = new PasswordInput('password_confirmation');
$passwordConf->setPlaceholder('Confirme a nova senha');

$hidden = new HiddenInput('token');
$hidden->setValue($token);

$span1 = new Span();
$span2 = new Span();
$span3 = new Span();

$span1->addChild($email);
$span1->addText('</br>');
$span2->addChild($password);
$span2->addText('</br>');
$span3->addChild($passwordConf);
$span3->addText('</br>');

$form = new BaseForm(URL::route('password.reset', $token));
$form->addChild($span1);
$form->addChild($span2);
$form->addChild($span3);
$form->addChild($hidden);
$form->addText('</br>');
$form->addChild($button);

echo $form->draw();
?>
@stop