@section('edit')
<br/>
<br/>
<p>
<form class="form-horizontal" action="{{ URL::route('songs.update', $song->id) }}" method="POST">
  <div class="control-group">
    <label class="control-label" for="inputName">Nome</label>
    <div class="controls">
      <input type="text" name="edtName" id="inputName" value="{{ $song->name }}" readonly>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputArtist">Artista</label>
    <div class="controls">
      <input type="text" name="edtArtist" id="inputArtist" value="{{ $song->band->name }}" readonly>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputLink">Link</label>
    <div class="controls">
      <input type="text" name="edtLink" id="inputLink" value="{{ $song->link }}">
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputVideo">Vídeo</label>
    <div class="controls">
      <input type="text" name="edtVideo" id="inputVideo" value="{{ $song->video }}">
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputLevel">Dificuldade</label>
    <div class="controls">
      <?php
      $cbb = new ComboBox('cbbLevel');
      $cbb->setId('inputLevel');
      $cbb->addChild(new Option('Escolha a Dificuldade'));
      for ($index = 1; $index <= 7; $index++) {
        $item = $cbb->addItem(strval($index), $index);
        $item->checked = ($index == $song->level);
      }
      echo $cbb->draw();
      ?>
    </div>
  </div>
  <div class="control-group">
    <div class="controls">
      <button type="submit" class="btn btn-primary">Salvar</button>
    </div>
  </div>
</div>
</form>
</p>
@stop