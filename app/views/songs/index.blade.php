@extends('master')

@section('content')
<h2>Lista de Músicas para Download!</h2>
<br/>
<br/>
@yield('songs.notlogged')
<h3>
  AJUDE O JOGO
</h3>
<p>
  Não poste músicas que já existam no jogo, mesmo que a chart seja diferente. Os jogadores precisam de músicas diferentes para jogar, então ajude-nos com músicas novas que ainda não foram colocadas no jogo.
  Além disso, se você achar que esta música pode vir a entrar no jogo um dia, divulgue à outros jogadores para que ela possa vir a efetivamente entrar no jogo. O objetivo do Guitar Flash é disponibilizar a maior variedade possível de músicas para serem disputadas no ranking oficial!
</p>
@yield('insert')
@yield('search')
@yield('edit')
<br/>
<br/>  
<br/>
@yield('show')
@yield('songs.list')

@stop