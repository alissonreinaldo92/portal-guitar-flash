@extends('master')
@section('content')
<div class="container">
  <div class="row-fluid">
    <div class="span9">
      @yield('resetpassword')
    </div>
  </div>
</div>

<!-- @yield('facebook.posts') -->

<!-- Main hero unit for a primary marketing message or call to action -->
<div class="hero-unit">
  <h1>Portal do Guitar Flash</h1>
  </br>
  <p>
    Seja bem-vindo(a) ao novo portal do guitar flash, onde você poderá encontrar diversas músicas customizáveis para jogar.
  </p>
  </br>
  <p>Guitar Flash é um jogo inspirado no Guitar Hero, desenvolvido pelo Allan Bueno. Nele, você precisa usar seu teclado para tocar as notas da música conforme o ritmo.
    Para maiores detalhes, acesse o site <a href='http://www.guitarflash.me/' target="_blank"><b>http://www.guitarflash.me/</b></a> e vá na aba "Ajuda", lá você encontrará maiores informações e também poderá jogar e conhecer melhor o jogo, que hoje ultrapassa a casa de milhões de jogadores.
  </p>
  </br>
  <p>
    Hoje existe uma versão online na qual você pode jogar suas próprias músicas, e o objetivo deste portal é disponibilizar aos jogadores um local para compartilhá-las.
    Aqui você poderá encontrar músicas para download, bem como postar suas próprias (caso você seja um charter, ou esteja convertendo músicas de outros jogos como Frets On Fire, por exemplo).
  </p>
  </br>
  <p>
    Para acessar o <b>Guitar Flash Custom 2.0</b>, acesse aqui:
  </p>
  <p>
    <?php
    $anchor = new Anchor('http://www.guitarflash.me/custom');
    $anchor->addStyle(TypeStyleTBButton::Base);
    $anchor->addStyle(TypeStyleTBButton::Inverse);
    $anchor->addStyle(TypeStyleTBButton::Large);
    $anchor->setAttribute('role', 'button');
    $anchor->setTarget('_blank');
    $anchor->addChild(new Icon(TypeStyleTBIcons::DownloadWhite));
    $anchor->addChild(new Text('Guitar Flash Custom 2.0'));
    echo $anchor->draw();
    ?>
  </p>
  </br>
  <p>
    Para acessar o <b>Guitar Flash Custom 1.0</b> (este geralmente utilizado para converter arquivos .chart ou .midi - formatos do Frets On Fire, Guitar Hero - para .sng ou .xml), acesse aqui:
  </p>
  <p>
    <?php
    $anchor2 = new Anchor('http://www.mediafire.com/download/brtc8evgcbk82f4/Guitar_Flash_Custom.rar');
    $anchor2->addStyle(TypeStyleTBButton::Base);
    $anchor2->addStyle(TypeStyleTBButton::Inverse);
    $anchor2->addStyle(TypeStyleTBButton::Large);
    $anchor2->setAttribute('role', 'button');
    $anchor2->setTarget('_blank');
    $anchor2->addChild(new Icon(TypeStyleTBIcons::DownloadWhite));
    $anchor2->addChild(new Text('Guitar Flash Custom 1.0'));
    echo $anchor2->draw();
    ?>
  </p>
</div>
@stop