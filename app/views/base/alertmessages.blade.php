@section('alertmessages')
<?php
try {
  $alert = new Alert();
  $alert->setAlertMessage($alertMessages);
  $alert->constructCloseButton();
  echo $alert->Draw();
} catch (Exception $e) {
  echo $e->getMessage();
}
?>
@stop