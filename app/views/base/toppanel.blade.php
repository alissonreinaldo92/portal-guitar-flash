@section('toppanel')
<?php
try {
  $navBar = new NavBar;
  $menu = new Menu;

  $navBar->addStyle(TypeStyleTBNavBar::Inverse);
  $menu->addStyle(TypeStyleTBList::Nav);
  $menu->addItem('Home', URL::route('Home'), new Icon(TypeStyleTBIcons::HomeWhite));
//  $menu->addItem('Guitar Flash da Depressão', URL::route('guitarflashdepressao.index'), new Icon(TypeStyleTBIcons::HomeWhite));
  if (!LoginChain::hasLoggedUser()) 
    $menu->addItem('Cadastrar', URL::route('users.register'), new Icon(TypeStyleTBIcons::PlusSignWhite));
  else
    $menu->addItem('Perfil', URL::route('users.index'), new Icon(TypeStyleTBIcons::UserWhite));

  $menu->addItem('Músicas', URL::route('songs.index'), new Icon(TypeStyleTBIcons::MusicWhite));
  $menu->addItem('Bandas', URL::route('bands.index'), new Icon(TypeStyleTBIcons::HeadphoneWhite));
  $menu->addItem('Usuários', URL::route('users.list'), new Icon(TypeStyleTBIcons::UserWhite));
//  $menu->addItem('Paths', URL::route('Paths'));
  $menu->addItem('Conversor', URL::route('conversor.index'), new Icon(TypeStyleTBIcons::WrenchWhite));
//  $menu->addItem('Forum', URL::route('Forum'));

  $navBar->addChild($menu);
  echo $navBar->draw();
} catch (Exception $exc) {
  throw new BadMethodCallException('Erro ao criar MainNav: ' . $exc->getMessage() . ' - ' . get_class($exc));
}
?>

@stop